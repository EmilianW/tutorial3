package emilianwilczek;

public class Unit
{
    private String unitCode;
    private String unitTitle;
    private Student classList[] = new Student[10];
    private String result[] = new String[10];

    public Unit(String unitCode, String unitTitle)
    {
        this.unitCode = unitCode;
        this.unitTitle = unitTitle;
    }


    public String getUnitCode()
    {
        return unitCode;
    }

    public String getUnitTitle()
    {
        return unitTitle;
    }

    public void addStudent(Student newStudent)
    {
        boolean freeSpace = false;
        for(int i = 0; i < classList.length; i++)
        {
            if(classList[i] == null)
            {
                classList[i] = newStudent;
                freeSpace = true;
                break;
            }
        }
        if (freeSpace)
        {
            System.out.println("Student inserted");
        }
        else
        {
            System.out.println("No space available");
        }
    }
}
